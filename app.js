const express = require('express')
const cors = require('cors')
const logger = require('morgan')
const createError = require('http-errors')
const path = require('path')

const routesApp = require(path.join(__dirname, 'routes'))
const config = require(path.join(__dirname, 'config.json'))

const corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200
}
const app = express()

app.use(cors(corsOptions))
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(routesApp)
app.set('port', process.env.PORT || config.port || 3000)

app.use((req, res, next) => {
  	next(createError(404))
})
app.use((err, req, res, next) => {
	res.locals.message = err.message
	res.locals.error = req.app.get('env') === 'development' ? err : {}

	res.status(err.status || 500)
	res.json({
        message: err.message
    })
})

app.listen(app.get('port'), () => {
	console.log('API empresas-backend na porta', app.get('port') + '.')
})