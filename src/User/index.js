const path = require('path')
const _ = require('lodash')

const mysqlConn = require(path.join(__dirname, '..', 'Util', 'mysqlConn'))
const userColumns = require('./userColumns')

const makeQueryUser = params => {
	let query = 'SELECT ' + _.join(userColumns, ',') + ' FROM user '

 	if (!_.isEmpty(params)){
 		let whereArr = []

 		for (let column in params){
 			whereArr.push(column + ' = "' + params[column] + '"')
 		}

 		query += 'WHERE ' + _.join(whereArr, ' AND ')
 	}    

	return query
}

const methods = {
	findUser: async params => {
		return await mysqlConn.executeQuery(makeQueryUser(params))
	}
}

module.exports = methods