const path = require('path')
const _ = require('lodash')

const mysqlConn = require(path.join(__dirname, '..', 'Util', 'mysqlConn'))
const enterpriseConlumns = require('./enterpriseColumns')

const makeQueryEnterprise = params => {
	let query = 'SELECT ' + _.join(enterpriseConlumns, ',') + ' FROM enterprise '

 	if (!_.isEmpty(params)){
 		let whereArr = []

 		for (let column in params){
 			whereArr.push(column !== 'enterprise_name' ? 
 				column + ' = "' + params[column] + '"' :
 				'UPPER(' + column + ') LIKE "%' + _.toUpper(params[column]) + '%"')
 			
 		}

 		query += 'WHERE ' + _.join(whereArr, ' AND ')
 	}    

	return query
}

const methods = {
	findEnterprises: async params => {
		return await mysqlConn.executeQuery(makeQueryEnterprise(params))
	}
}

module.exports = methods