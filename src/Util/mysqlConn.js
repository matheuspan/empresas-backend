const mysql = require('mysql2')
const path = require('path')
const Promise = require('bluebird')

const config = require(path.join(__dirname, '..', '..', 'config.json'))

const connection = mysql.createConnection(config.mysql)

connection.connect((err) => {
	// Falha ao conectar com MySQL
	if (err) throw err

	console.log('Conectado ao MySQL')
})

const methods = {
	executeQuery: async query => {
		return new Promise((resolve, reject) => {
			connection.query(query, (err, results, fields) => {
				if (err) reject(err)

				resolve(results)
			})
		})
	}
}

module.exports = methods