const routes = require('express').Router()
const path = require('path')
const _ = require('lodash')

const config = require(path.join(__dirname, '..', '..', 'config.json'))
const tokenValidation = require(path.join(__dirname, 'tokenValidation'))
const enterprise = require(path.join(__dirname, '..', '..', 'src', 'Enterprise'))

routes.get(`/api/${config.api.version}/enterprises`, tokenValidation, async (req, res) => {
	let enterpriseTypes = _.get(req.query, 'enterprise_types', false)
	let enterpriseName = _.get(req.query, 'name', false)

	let params = {}
	// Busca personalizada
	if (enterpriseTypes) params.enterprise_type = enterpriseTypes
	if (enterpriseName) params.enterprise_name = enterpriseName

	try {
		let enterprisesFound = await enterprise.findEnterprises(params)
		res.json({enterprises: enterprisesFound})
	} catch (e) {
		// Erro na busca das empresas
		res.status(500).send()
	}
})

const defaultSuccessResponse = enterprise => {
	return {
		enterprise,
		success: true
	}
}

const defaultErrorResponse = () => {
	return {
	    status: '404',
    	error: 'Not Found'
	}
}

routes.get(`/api/${config.api.version}/enterprises/:id`, tokenValidation, async (req, res) => {
	try {
		let enterpriseFound = await enterprise.findEnterprises({id: req.params.id})
		
		if (!_.isEmpty(enterpriseFound)) return res.json(defaultSuccessResponse(enterpriseFound[0]))
	} catch (e) {
		// Erro na busca das empresas
	}

	res.status(404).json(defaultErrorResponse())
})

module.exports = routes