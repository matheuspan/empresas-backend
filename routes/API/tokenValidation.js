const path = require('path')
const _ = require('lodash')
const jwt = require('jsonwebtoken')

const user = require(path.join(__dirname, '..', '..', 'src', 'User'))
const config = require(path.join(__dirname, '..', '..', 'config.json'))

const badTokenValidationResponse = () => {
	return {
		errors: [
		    'Authorized users only.'
		]
	}
}

const tokenValidation = async (req, res, next) => {
	let accessToken = _.get(req.headers, 'access-token', false)
	let client = _.get(req.headers, 'client', false)
	let uid =  _.get(req.headers, 'uid', false)

	if (accessToken && client && uid) {
		try {
			let userFound = await user.findUser({email: uid})

			let decodedToken = jwt.verify(accessToken, config.jwt.salt)
			let decodedClient = jwt.verify(client, config.jwt.salt)

			if (userFound.id === decodedToken.id && decodedClient.uid === uid) return next()
		} catch (e) {
			// Erro na autenticação dos headers
		}
	}	

	res.status(401).json(badTokenValidationResponse())
}

module.exports = tokenValidation