const express = require('express')
const path = require('path')

const authRoutes = require(path.join(__dirname, 'auth'))
const enterprisesRoutes = require(path.join(__dirname, 'API', 'enterprises'))

const routesApp = express()

routesApp.use('/', authRoutes)
routesApp.use('/', enterprisesRoutes)

module.exports = routesApp