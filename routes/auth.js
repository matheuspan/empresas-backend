const path = require('path')
const routes = require('express').Router()
const _ = require('lodash')
const jwt = require('jsonwebtoken')

const user = require(path.join(__dirname, '..', 'src', 'User'))
const config = require(path.join(__dirname, '..', 'config.json'))

const defaultSuccessResponse = user => {
	return {
		investor: user,
		success: true,
		enterprise: null
	}
}

const defaultErrorResponse = () => {
	return {
	    success: false,
	    errors: [
	        'Invalid login credentials. Please try again.'
	    ]
	}
}

routes.post(`/api/${config.api.version}/users/auth/sign_in`, async (req, res, next) => {
	let email = _.get(req.body, 'email', false)
	let password = _.get(req.body, 'password', false)	

	if (email && password){
		let userFound = await user.findUser({email, password})
		
		if (!_.isEmpty(userFound)){
			userFound = userFound[0]
			// Cria tokens com jwt
			let accessToken = jwt.sign({id: userFound.id}, config.jwt.salt, config.jwt.options)
			let client = jwt.sign({uid: email}, config.jwt.salt, config.jwt.options)

			res.set({
				'access-token': accessToken,
				client,
				'uid': email,
				'expiry': Math.floor(Date.now() / 1000) + 86400,
				'token-type': 'Bearer'
			})

			return res.json(defaultSuccessResponse(userFound))			
		}
	}

	// Falha ao realizar Login
	res.status(401).json(defaultErrorResponse())
})

module.exports = routes